package com.mateimadalin.urlchangecheck.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.mateimadalin.urlchangecheck.Objects.URL;
import com.mateimadalin.urlchangecheck.R;
import com.mateimadalin.urlchangecheck.URLChangeListener;
import java.util.List;

public class URLAdapter extends ArrayAdapter<URL> implements View.OnClickListener {

    private List<URL> urlObjectList;
    private Context parentContext;

    private static class URLHolder {
        TextView title;
        TextView url;
        TextView lastModified;
    }

    public URLAdapter(List<URL> urlObjectList, Context parentContext){
        super(parentContext, R.layout.url_list_item, urlObjectList);
        this.urlObjectList = urlObjectList;
        this.parentContext = parentContext;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final URLHolder urlHolder;
        final URL url = getItem(position);

        if (convertView == null) {

            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.url_list_item, parent, false);
            urlHolder = new URLHolder();
            urlHolder.title = convertView.findViewById(R.id.urlTitle);
            urlHolder.url = convertView.findViewById(R.id.url);
            urlHolder.lastModified = convertView.findViewById(R.id.lastModified);
            convertView.setTag(urlHolder);

        }else{
            urlHolder = (URLHolder) convertView.getTag();
        }

        urlHolder.title.setText(url.getName());
        urlHolder.url.setText(url.getURL());
        String date = url.getLastModified() == null ? "no modifications" : url.getLastModified().toString();
        urlHolder.lastModified.setText(date);

        new Thread() {
            public void run() {
                while(true){
                    try {
                        Thread.sleep(5000);
                        new URLChangeListener(url).execute(url.getURL()).get();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();

        return convertView;
    }

}
