package com.mateimadalin.urlchangecheck;

import android.os.AsyncTask;

import com.mateimadalin.urlchangecheck.Activities.MainActivity;
import com.mateimadalin.urlchangecheck.Objects.DataHolder;
import com.mateimadalin.urlchangecheck.Objects.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Date;

public class URLChangeListener extends AsyncTask<String, Void, String> {
    private URL _url;

    public URLChangeListener(URL url) {
        _url = url;
    }

    @Override
    protected String doInBackground(String... params) {
        String url = params[0];
        try {
            String res = Jsoup.connect(url).userAgent("Mozilla/5.0").get().getElementsByTag("div").html()
                    .replaceAll("<a.*>","")
                    .replaceAll("<img.*>","")
                    .replaceAll("for=\".*\"","")
                    .replaceAll("<!--.*-->","")
                    .replaceAll("<input.*>","");

            return res;
        } catch (Exception e) {
            return null;
        }
    }


    protected void onPostExecute(String result) {
        if (result != null)
        {
            String currentMd5 = Util.md5(result);
            if(_url.getPageMD5() == null){
                _url.setPageMD5(currentMd5);
            }

            if(!_url.getPageMD5().equals(currentMd5)){
                _url.setLastModified(new Date());
                _url.setPageMD5(currentMd5);
                DataHolder.urlAdapter.notifyDataSetChanged();
                MainActivity.addNotification(_url);
            }
            _url.incrementCheckCount();

        }
    }
}