package com.mateimadalin.urlchangecheck.Activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mateimadalin.urlchangecheck.Objects.DataHolder;
import com.mateimadalin.urlchangecheck.Objects.URL;
import com.mateimadalin.urlchangecheck.R;
import com.mateimadalin.urlchangecheck.Util;

public class URLActivity extends AppCompatActivity {

    private URL globalURL;
    private Context context;

    private EditText name;
    private EditText url;
    private TextView lastModified;
    private TextView firstModified;
    private TextView countModified;
    private Button saveButton;
    private Button deleteButton;


    private void init(){
        this.name = findViewById(R.id.urlActivity_name);
        this.url = findViewById(R.id.urlActivity_url);
        this.lastModified = findViewById(R.id.urlActivity_lastModified);
        this.firstModified = findViewById(R.id.urlActivity_firstModified);
        this.countModified = findViewById(R.id.urlActivity_count);
        this.saveButton = findViewById(R.id.saveButton);
        this.deleteButton = findViewById(R.id.deleteButton);

        name.addTextChangedListener(textWatcher());
        url.addTextChangedListener(textWatcher());
        saveButton.setOnClickListener(saveOnClickListener());
        deleteButton.setOnClickListener(deleteOnClickListener());
    }

    private View.OnClickListener saveOnClickListener(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String currentName = name.getText().toString();
                String currentUrl = url.getText().toString();
                if(Util.validateName(currentName) && Util.validateURL(currentUrl)){
                    if(globalURL.getNewObj()){
                        globalURL.setNewObj(false);
                        DataHolder.urlObjectList.add(globalURL);
                    }

                    Integer index = DataHolder.urlObjectList.indexOf(globalURL);
                    DataHolder.urlObjectList.get(index).setName(currentName);
                    DataHolder.urlObjectList.get(index).setURL(currentUrl);
                    finishThis();
                }else{
                    Toast.makeText(context, "The name or the url is not valid. Try again.", Toast.LENGTH_LONG).show();
                }
            }
        };
    }

    private View.OnClickListener deleteOnClickListener(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataHolder.removeElement(globalURL);
                DataHolder.urlAdapter.notifyDataSetChanged();
                finishThis();
            }
        };
    }

    private TextWatcher textWatcher(){
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                saveButton.setVisibility(View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
    }

    private void setObjectAsLayout(URL url){
        this.name.setText(url.getName());
        this.url.setText(url.getURL());
        this.lastModified.setText(url.getLastModified() == null ? "no modifications" : url.getLastModified().toString());
        this.firstModified.setText(url.getFirstDate().toString());
        this.countModified.setText(url.getCheckCount().toString());
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_url);
        context = this.getApplicationContext();
        init();
        Intent intent = getIntent();
        globalURL = (URL) intent.getSerializableExtra("url");
        setObjectAsLayout(globalURL);
    }


    private void finishThis() {
        DataHolder.urlAdapter.notifyDataSetChanged();
        this.finish();
    }

}
