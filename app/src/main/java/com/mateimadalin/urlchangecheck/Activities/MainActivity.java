package com.mateimadalin.urlchangecheck.Activities;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mateimadalin.urlchangecheck.Adapters.URLAdapter;
import com.mateimadalin.urlchangecheck.Objects.DataHolder;
import com.mateimadalin.urlchangecheck.Objects.URL;
import com.mateimadalin.urlchangecheck.R;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static Context context;
    private ListView urlList;
    private Toolbar toolbar;
    private FloatingActionButton fab;

    private static final String PREFS_TAG = "SharedPrefs";
    private static final String PRODUCT_TAG = "MyURLs";

    private List<URL> getDataFromSharedPreferences(){
        Gson gson = new Gson();
        List<URL> productFromShared = new ArrayList<>();
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(PREFS_TAG, Context.MODE_PRIVATE);
        String jsonPreferences = sharedPref.getString(PRODUCT_TAG, "");
        Type type = new TypeToken<List<URL>>() {}.getType();
        productFromShared = gson.fromJson(jsonPreferences, type);
        return productFromShared;
    }

    private void setDataFromSharedPreferences(List<URL> curProduct){
        Gson gson = new Gson();
        String jsonCurProduct = gson.toJson(curProduct);
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(PREFS_TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(PRODUCT_TAG, jsonCurProduct);
        editor.commit();
    }

    private View.OnClickListener addNewUrlButtonClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startURLActivity(new URL("Name", "URL", true));
            }
        };
    }

    private AdapterView.OnItemClickListener urlListClickListener() {
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startURLActivity(DataHolder.urlObjectList.get(position));
            }
        };
    }


    private void init() {
        urlList = findViewById(R.id.urlList);
        toolbar = findViewById(R.id.toolbar);
        fab = findViewById(R.id.fab);
        setSupportActionBar(toolbar);

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this.getApplicationContext();
        init();
        List<URL> urlListFromPrefs = getDataFromSharedPreferences();
        if(urlListFromPrefs != null){
            DataHolder.urlObjectList = urlListFromPrefs;
        }else{
            DataHolder.urlObjectList = new ArrayList<>();
        }
        fab.setOnClickListener(addNewUrlButtonClickListener());
        DataHolder.urlAdapter = new URLAdapter(DataHolder.urlObjectList, context);
        urlList.setAdapter(DataHolder.urlAdapter);
        urlList.setOnItemClickListener(urlListClickListener());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void startURLActivity(URL url) {
        url.setPage("");
        Intent intent = new Intent(this, URLActivity.class);
        intent.putExtra("url", (Serializable) url);
        startActivity(intent);
    }

    @Override
    protected void onPause() {
        setDataFromSharedPreferences(DataHolder.urlObjectList);
        super.onPause();
    }

    public static void addNotification(URL url) {
        String urlS = url.getURL();
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(urlS));
        showNotification(context, "New change on your website", "We found changes on " + url.getName(), intent);
    }

    public static void showNotification(Context context, String title, String body, Intent intent) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        int notificationId = 1;
        String channelId = "channel-01";
        String channelName = "Channel Name";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(body);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntent(intent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        mBuilder.setContentIntent(resultPendingIntent);

        notificationManager.notify(notificationId, mBuilder.build());
    }

}
