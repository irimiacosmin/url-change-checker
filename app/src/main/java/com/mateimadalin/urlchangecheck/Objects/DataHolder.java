package com.mateimadalin.urlchangecheck.Objects;

import com.mateimadalin.urlchangecheck.Adapters.URLAdapter;
import com.mateimadalin.urlchangecheck.Objects.URL;

import java.util.ArrayList;
import java.util.List;

public class DataHolder {
    public static List<URL> urlObjectList = new ArrayList<>();
    public static URLAdapter urlAdapter;

    public static boolean removeElement(URL url){
        if(urlObjectList.contains(url)){
            urlObjectList.remove(url);
            return true;
        }else{
            return false;
        }
    }
}
