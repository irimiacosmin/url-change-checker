package com.mateimadalin.urlchangecheck.Objects;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class URL implements Serializable {

    private String name;
    private String URL;
    private Date lastModified;
    private Date firstDate;
    private Integer checkCount;
    private String pageMD5;
    private String page;
    private Boolean newObj;

    public URL(String name, String URL) {
        this.name = name;
        this.URL = URL;
        firstDate = new Date();
        newObj = false;
    }

    public URL(String name, String URL, Boolean newObj) {
        this.name = name;
        this.URL = URL;
        firstDate = new Date();
        this.newObj = newObj;
    }

    public String getName() {
        return name;
    }

    public String getURL() {
        return URL;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public void incrementCheckCount() {
        if(this.checkCount == null){
            this.checkCount = 0;
        }
        this.checkCount++;
    }

    public Boolean getNewObj() {
        return newObj;
    }

    public void setNewObj(Boolean newObj) {
        this.newObj = newObj;
    }

    public String getPageMD5() {
        return pageMD5;
    }

    public void setPageMD5(String pageMD5) {
        this.pageMD5 = pageMD5;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public Date getFirstDate() {
        return this.firstDate;
    }

    public Integer getCheckCount() {
        if(this.checkCount == null){
            return 0;
        }
        return checkCount;
    }

    @Override
    public String toString() {
        return "URL{" +
                "name='" + name + '\'' +
                ", URL='" + URL + '\'' +
                ", lastModified=" + lastModified +
                ", firstDate=" + firstDate +
                ", checkCount=" + checkCount +
                ", pageMD5='" + pageMD5 + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        URL url = (URL) o;
        return Objects.equals(name, url.name) &&
                Objects.equals(URL, url.URL);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, URL);
    }
}